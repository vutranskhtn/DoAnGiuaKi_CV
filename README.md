# This Project
### Required
#### Install NODEJS for Windows, Ubuntu, MacOSX:
* https://nodejs.org/en/download/

#### Check node version
```sh
$ node -v
```
#### Check npm version
```sh
$ npm -v
```
#### Install BOWER
```sh
$ [sudo] npm install -g bower
```
#### Check bower version
```sh
$ bower -v
```
    
### Installation
You need Gulp installed globally:
```sh
$ [sudo] npm install -g gulp
```
Install NPM package
```sh
$ [sudo] npm install
```
Install BOWER package
```sh
$ bower install
```
### Run without minify
```sh
$ gulp serve
```
### Run with minify
```sh
$ gulp serve:dist
```